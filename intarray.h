#ifndef INTARRAY
#define INTARRAY

//Класс IntArray
//Обертка для массива целочисленных переменных
//с возможностью изменения размера массива,
//удаления элементов и добавления элемента
//в конец массива
class IntArray
{
    //То, что доступно всем
    //(и самому классу, и его пользователям)
    public:
        //Возращает размер массива
        int getSize() const;
        //Меняет размер массива,
        //сохраняя данные по возможности
        void setSize(int n);

        //Получает текущее количество
        //созданных объектов
        static int getArrCount()
        {
            return arrCount;
        }

        //Метод, возвращающий
        //значение элемента массива
        //с индексом index
        //!!!TODO!!!
        //! исправить ошибку, когда index > size
        int getElem(int index) const;

        //Метод, задающий
        //значение элемента массива
        //с индексом index
        void setElem(int index, int val);

        //Метод, удаляющий элемент
        //с индексом index
        void deleteElem(int index);

        //Метод, добавляющий элемент
        //в конец массива
        void addElem(int val);

        //Метод, добавляющий элемент
        //в произвольное место массива
        void addElemMiddle(int index, int val);

        //Метод, сравнивающий текущий массив
        //с другим по размеру
        IntArray& intArrayCompare(IntArray& anotherArr);

        //Конструкторы
        //Конструктор по умолчанию
        IntArray();
        IntArray(int n, bool random = false);
        IntArray(int n, const int* arr);
        //Копирующий конструктор
        IntArray(const IntArray& sec_arr);

        //Деструктор
        ~IntArray();

        //Метод, выводящий массив на консоль
        void print() const;



    //То, что доступно только внутри класса
    private:

        //Статическая переменная
        //Подсчитывает количество объектов
        //данного класса
        static int arrCount;

        //Текущий размер массива
        int size;

        //Указатель на сам массив
        int* array;

    //То, что доступно внутри класса
    //+доступно наследникам и друзьям
    //protected:

};

#endif // INTARRAY

